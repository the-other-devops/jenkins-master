build:
	@docker-compose -p jenkins build

run:
	@docker-compose -p jenkins up -d nginx jenkins-data jenkins-master

stop:
	@docker-compose -p jenkins stop

clean: stop
	@docker-compose -p jenkins rm nginx jenkins-master

clean-data: stop
	@docker-compose -p jenkins rm -v jenkins-data

clean-all: stop
	@docker-compose -p jenkins rm -v nginx jenkins-master jenkins-data

clean-images:
	@docker rmi $(docker images --quiet --filter "dangling=true")
